import numpy as np
import cv2
import imutils
import pytesseract as ocr
import sys
from matplotlib import pyplot as plt


print ('Number of Arguments:', len(sys.argv))
print ('Arguments:', sys.argv[1])

img = transform_imagem(sys.argv[1])
    img_final = processamento_img(img)
    mostrar(img_final)

texto = ocr.image_to_string(img_final, lang="por", config=config_tesseract)
print(texto)



def ordenar_pontos(pontos):
  pontos = pontos.reshape((4,2))
  pontos_novos = np.zeros((4, 1, 2), dtype=np.int32)

  add = pontos.sum(1)
  pontos_novos[0] = pontos[np.argmin(add)]
  pontos_novos[2] = pontos[np.argmax(add)]

  dif = np.diff(pontos, axis = 1)
  pontos_novos[1] = pontos[np.argmin(dif)]
  pontos_novos[3] = pontos[np.argmax(dif)]

  return pontos_novos

def transform_imagem(nome_imagem):
  img = cv2.imread(nome_imagem)
  original = img.copy()
  (H, W) = img.shape[:2]

  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  blur = cv2.GaussianBlur(gray, (7, 7), 0)
  edged = cv2.Canny(blur, 60, 160)
  conts = encontrar_contornos(edged.copy())
  for c in conts:
    peri = cv2.arcLength(c, True)
    aprox = cv2.approxPolyDP(c, 0.02 * peri, True)

    if len(aprox) == 4:
      maior = aprox
      break

  cv2.drawContours(img, maior, -1, (120, 255, 0), 28)
  cv2.drawContours(img, [maior], -1, (120, 255, 0), 2)


  pontosMaior = ordenar_pontos(maior)
  pts1 = np.float32(pontosMaior)
  pts2 = np.float32([[0, 0], [W, 0], [W, H], [0, H]])

  matriz = cv2.getPerspectiveTransform(pts1, pts2)
  transform = cv2.warpPerspective(original, matriz, (W, H))


  return transform


  def processamento_img(img):
    img_process = cv2.resize(img, None, fx=1.6, fy=1.6, interpolation=cv2.INTER_CUBIC)
    img_process = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_process = cv2.adaptiveThreshold(img_process, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 9)
    return img_process

