package com.nassiesse.ocr.dto;

public class ResponseDTO {
    private String nomeDocumento;
    private double porcentagem;
    private String texto;

    public ResponseDTO(String nomeDocumento, double porcentagem, String texto) {
        this.nomeDocumento = nomeDocumento;
        this.porcentagem = porcentagem;
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getNomeDocumento() {
        return nomeDocumento;
    }

    public void setNomeDocumento(String nomeDocumento) {
        this.nomeDocumento = nomeDocumento;
    }

    public double getPorcentagem() {
        return  Math.round(porcentagem);
    }

    public void setPorcentagem(double porcentagem) {
        this.porcentagem = porcentagem;
    }

    @Override
    public String toString() {
        return "ResponseDTO{" +
                ", nomeDocumento='" + nomeDocumento + '\'' +
                ", porcentagem=" + porcentagem +
                '}';
    }
}
