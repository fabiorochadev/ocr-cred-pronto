package com.nassiesse.ocr.controller;

import com.nassiesse.ocr.service.OcrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class SimpleOCRController {
    @Autowired
    private OcrService service;


    @PostMapping("/api/pdf/extractText")
    public @ResponseBody
    ResponseEntity<String>
    uploadFile(@RequestParam("file") MultipartFile file) {
        try {

            var responseJson = service.extractTextFromFile(file);

            return new ResponseEntity<>(responseJson.toString(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/api/pdf/ping")
    public ResponseEntity<String> get() {
        return new ResponseEntity<>("PONG", HttpStatus.OK);
    }

}
