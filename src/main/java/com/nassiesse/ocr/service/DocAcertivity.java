package com.nassiesse.ocr.service;

import com.nassiesse.ocr.dto.ResponseDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Component
public class DocAcertivity {

   private ResponseDTO calcularAcertividadeCertidaoCasamento(String result) {

        var palavrasChaves = Arrays.asList("REPÚBLICA FEDERATIVA DO BRASILREGISTRO CIVIL DAS PESSOAS NATURAIS",
                "REPÚBLICA FEDERATIVA",
                "REPÚBLICA FEDERATIVA DO BRASIL",
                "FEDERATIVA DO BRASIL",
                "REGISTRO CIVIL DAS PESSOAS NATURAIS",
                "REGISTRO CIVIL",
                "CERTIDÃO DE CASAMENTO",
                "CERTIDÃO", "CASAMENTO",
                "NOME QUE CADA UM DOS CÔNJUGES PASSOU A UTILIZAR",
                "CÔNJUGES",
                "CARTÓRIO",
                "REGIME DE BENS DO CASAMENTO");

        double porcentagem = calcularPorcentagem(result, palavrasChaves);


        return new ResponseDTO("Certidão de Casamento", porcentagem, result);
    }

    private ResponseDTO calcularAcertividadeMatricula(String result) {

        var palavrasChaves = Arrays.asList("REGISTRO GERALmatrícula ficha",
                "matrícula",
                "ficha",
                "REGISTRO GERAL",
                "Localizado ho",
                "Localizado no",
                "Localizado",
                "INSCRIÇÃO CADASTRAL", "PROPRIETÁRIA",
                "CADASTRAL",
                "VENDA E COMPRA",
                "Inscrição",
                "REGISTRO ANTERIOR",
                "CADASTRO");

        double porcentagem = calcularPorcentagem(result, palavrasChaves);


        return new ResponseDTO("Matrícula de imóvel", porcentagem, result);
    }


    private ResponseDTO calcularAcertividadIptu(String result) {

        var palavrasChaves = Arrays.asList("REFEITURA DE",
                "SECRETARIA DAFAZENDA",
                "SECRETARIA",
                "FAZENDA",
                "IPTU",
                "Contribuinte",
                "AGÊNCIAS DA CAIXA",
                "INSCRIÇÃO CADASTRAL", "PROPRIETÁRIA",
                "Responsabilidade do Cedente",
                "R$");

        double porcentagem = calcularPorcentagem(result, palavrasChaves);


        return new ResponseDTO("IPTU do imóvel", porcentagem, result);
    }


    private double calcularPorcentagem(String result, List<String> palavrasChaves) {
        double acertos = 0;
        for (String p : palavrasChaves) {
            if (StringUtils.countOccurrencesOf(result.toLowerCase(), p.toLowerCase()) > 0) {
                acertos++;
            }
        }
        return ((acertos / palavrasChaves.size()) * 100);
    }


    public ResponseDTO getAcertivity(String result){
        List<ResponseDTO> responseDTOList = new ArrayList<>();

        responseDTOList.add(calcularAcertividadeCertidaoCasamento(result));
        responseDTOList.add(calcularAcertividadeMatricula(result));
        responseDTOList.add(calcularAcertividadIptu(result));

        return responseDTOList
                .stream()
                .max(Comparator.comparing(ResponseDTO::getPorcentagem))
                .orElseThrow(NoSuchElementException::new);
    }

}
