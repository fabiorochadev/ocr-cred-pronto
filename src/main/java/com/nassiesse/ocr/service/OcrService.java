package com.nassiesse.ocr.service;

import com.nassiesse.ocr.dto.ResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;

@Slf4j
@Service
public class OcrService {
    @Autowired
    private DocAcertivity detectText;

    public JSONObject
    extractTextFromFile(MultipartFile file) throws  IOException {
        // Load file into PDFBox class
        PDDocument document = PDDocument.load(file.getBytes());
        JSONObject response = new JSONObject();

        var responseDTO = extractTextFromScannedDocument(document);

        response.put("fileName", file.getOriginalFilename());
        if (responseDTO.getPorcentagem() <= 0) {
            response.put("docName", "Documento desconhecido");
        } else {
            response.put("docName", responseDTO.getNomeDocumento());
        }
        response.put("percentage", responseDTO.getPorcentagem());

        response.put("text", responseDTO.getTexto());
        return response;
    }

    private ResponseDTO extractTextFromScannedDocument(PDDocument document) throws IOException {
        StringBuilder result = new StringBuilder();
        PDFRenderer pdfRenderer = new PDFRenderer(document);

        for (int page = 0; page < document.getNumberOfPages(); page++) {

            BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);

            File temp = File.createTempFile("tempfile_" + page, ".png");
            ImageIO.write(bim, "png", temp);

            result.append(runScript(temp.getPath()));
            log.info("Caminho do arquivo " + temp.getPath());
            log.info("Conteudo do arquivo " + result);

            temp.delete();
        }
        document.close();
        return detectText.getAcertivity(result.toString());

    }


    public String runScript(String fiilePath) {
        StringBuilder sb = new StringBuilder();
        try {
            var process = Runtime.getRuntime().exec(new String[]{"python3", "/Users/lopes/Documents/readDocImage.py", fiilePath});
            InputStream stdout = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stdout, StandardCharsets.UTF_8));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

        } catch (Exception e) {
            log.error("Exception Raised" + e);
        }

        return sb.toString();
    }


}
